import os
import csv
import matplotlib.pyplot as plt

SYN = "0x00000002"
FIN = "0x00000011"
ACK = "0x00000010"

def pcap_to_csv(fname):
    os.system(f"tshark -n -r {fname}.pcap -E separator=\;  -E header=y -T fields \
                -e frame.time_epoch \
                -e ip.proto \
                -e ip.src \
                -e ip.dst \
                -e tcp.srcport \
                -e tcp.dstport \
                -e tcp.flags > {fname}.csv")
    return 0

def read_csv(fname):
    file = open(f'{fname}.csv', 'r')
    rd = csv.DictReader(file, delimiter=';')
    
    my_dict = {}
    
    for row in rd:
        if row["tcp.srcport"] == "80":
            port = row["tcp.dstport"]
        else:
            port = row["tcp.srcport"]

        if port not in my_dict:
            my_dict[port] = [row]
        else:
            my_dict[port].append(row)

    return my_dict

def get_time(conn_dict):
    origin_time = float(list(conn_dict.values())[0][0]['frame.time_epoch'])
    conn_times = {}
    for conn_i in conn_dict:
        start_time = end_time = -1
        for conn_j in conn_dict[conn_i]:
            if conn_j["tcp.flags"] == SYN:
                start_time = float(conn_j["frame.time_epoch"])
                break

        for j, conn_j in reversed(list(enumerate(conn_dict[conn_i]))):
            if j == 0:
                end_time = float(start_time) + 200 

            if conn_j["tcp.flags"] == ACK and conn_dict[conn_i][j-1]["tcp.flags"] == FIN:
                end_time = float(conn_j["frame.time_epoch"])
                break

        conn_times[conn_i] = {"start_time": start_time - origin_time, "duration": end_time - start_time}
    
    return conn_times

def save_and_plot(conn_times, fname):
    file = open(f"{fname}.csv", "w")
    x = []
    y = []
    for itm in conn_times:
        file.write(str(conn_times[itm]["start_time"]) + ";" + str(conn_times[itm]["duration"]) + "\n")
        x.append(conn_times[itm]["start_time"])
        y.append(conn_times[itm]["duration"])
    file.close()
    plt.scatter(x,y, marker='3', color='orange')
    #plt.plot(x,y)
    plt.xlabel("Starting time")
    plt.ylabel("Connection duration (s)")
    #plt.title("Connection delay during SYN flood (SYN Cookies off, IP spoofing on)")
    plt.show()


filename = input("Insert .pcap file name: ")

pcap_to_csv(filename)
rval = read_csv(filename)
time_dict=get_time(rval)
save_and_plot(time_dict, f"final_{filename}")
