#!/bin/sh

if [ $1 = "normal" ]; then
    timeout 180 ./send_traffic.sh $2
elif [ $1 = "attack" ]; then
    sleep 30
    timeout 120 ./attack.sh
fi
