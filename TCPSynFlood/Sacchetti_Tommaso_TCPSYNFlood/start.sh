#!/bin/sh

#Runs the three main scripts. 
#The Server TCPDUMP command is needed for the traffic analysis of some situations.
ssh client.tcpsynflood.offtech "./capture.sh & sleep 1 && ./sendtraffic.sh" & 
ssh server.tcpsynflood.offtech "sudo timeout 183 tcpdump -s 0 -nn -i any tcp -w server.pcap port 80" & 
ssh attacker.tcpsynflood.offtech "./delayedattack.sh" &
wait

echo "Simulation finished"