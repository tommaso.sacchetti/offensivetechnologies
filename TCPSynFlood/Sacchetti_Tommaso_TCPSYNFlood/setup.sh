#!/bin/sh

#Change the server IP accordingly to the NS file used.

#serverIP="12.1.4.1"
serverIP="5.6.7.8"

ssh server.tcpsynflood.offtech "/share/education/TCPSYNFlood_USC_ISI/install-server && sudo sysctl -w net.ipv4.tcp_syncookies=0 && sudo sysctl -w net.ipv4.tcp_max_syn_backlog=10000" & 
ssh attacker.tcpsynflood.offtech "/share/education/TCPSYNFlood_USC_ISI/install-flooder" & 
ssh client.tcpsynflood.offtech "ip route get $serverIP | awk '{printf \"%s\", \$5}' > iface" &
wait
