#!/bin/sh

#scp $1 otech2ab@users.isi.deterlab.net:/users/otech2ab/$1

#scp start.sh otech2ab@users.isi.deterlab.net:/users/otech2ab/start.sh

#scp setup.sh otech2ab@users.isi.deterlab.net:/users/otech2ab/setup.sh

#scp delayedattack.sh otech2ab@users.isi.deterlab.net:/users/otech2ab/delayedattack.sh

#scp sendtraffic.sh otech2ab@users.isi.deterlab.net:/users/otech2ab/sendtraffic.sh

#scp capture.sh otech2ab@users.isi.deterlab.net:/users/otech2ab/capture.sh

declare -a arr=("start.sh" "setup.sh" "delayedattack.sh" "sendtraffic.sh" "capture.sh")

for i in "${arr[@]}"
do
    scp $i otech2ab@users.isi.deterlab.net:/users/otech2ab/$i
done

ssh otech2ab@users.isi.deterlab.net "chmod +x *.sh"