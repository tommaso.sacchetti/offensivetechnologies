#!/bin/sh

#Remove old files & folders
rm -rf part_1 && mkdir part_1
rm -rf part_2 && mkdir part_2
rm -rf part_3 && mkdir part_3

#Delete the kernel route from asn2 & asn3
ssh asn2.sacchetti-bgphijack.offtech "sudo ip route del 10.1.1.0/24"
ssh asn3.sacchetti-bgphijack.offtech "sudo ip route del 10.1.1.0/24"

#Check the path client-server and download the README
ssh client.sacchetti-bgphijack.offtech "traceroute -n 10.1.1.2 > part_1/traceroute_out_client && netstat -rn > part_1/netstat_out_client && sudo vtysh -c \"show ip route\" > part_1/vtysh_out_client && cd part_1 && curl --user anonymous:qwerty --remote-name ftp://10.1.1.2/README"

#Display the contents of the BGP routing tables
ssh asn2.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_1/vtysh_out_asn2"
ssh asn3.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_1/vtysh_out_asn3"