#!/bin/sh

#To be executed in asn4 after the Telnet commands
#sudo iptables -t nat -F
#sudo iptables -t nat -A PREROUTING -d 10.1.1.2 -m ttl --ttl-gt 1 -j NETMAP --to 10.6.1.2
#sudo iptables -t nat -A POSTROUTING -s 10.6.1.2 -j NETMAP --to 10.1.1.2

#Check the path client-server and download the README
ssh client.sacchetti-BGPHijack.offtech "traceroute -n 10.1.1.2 > part_2/traceroute_out_client && cd part_2 && curl --user anonymous:qwerty --remote-name ftp://10.1.1.2/README"

#Display the contents of the BGP routing tables
ssh asn2.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_2/vtysh_out_asn2"
ssh asn3.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_2/vtysh_out_asn3"