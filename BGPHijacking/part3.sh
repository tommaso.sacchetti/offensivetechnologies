#!/bin/sh

#Check the route client-server and download the README
ssh client.sacchetti-BGPHijack.offtech "traceroute -n 10.1.1.2 > part_3/traceroute_out_client && cd part_3 && curl --user anonymous:qwerty --remote-name ftp://10.1.1.2/README"

#Display the contents of the BGP routing tables
ssh asn2.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_3/vtysh_out_asn2"
ssh asn3.sacchetti-bgphijack.offtech "sudo vtysh -c \"show ip bgp\" > part_3/vtysh_out_asn3"