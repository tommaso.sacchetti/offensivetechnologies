#!/bin/sh

ssh client.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"

ssh server.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"

ssh asn1.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"

ssh asn2.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"

ssh asn3.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"

ssh asn4.sacchetti-bgphijack.offtech "sudo /etc/init.d/quagga restart"