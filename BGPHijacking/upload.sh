#!/bin/sh

declare -a arr=("part1.sh" "part2.sh" "part3.sh")

for i in "${arr[@]}"
do
    scp $i otech2ab@users.isi.deterlab.net:/users/otech2ab/$i
done