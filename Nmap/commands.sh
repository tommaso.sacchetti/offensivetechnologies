#!/bin/sh

#ICMP echo ping only
sudo nmap -sn -PE 5.6.7.0/24

#TCP ACK scan
sudo nmap -sA 5.6.7.0/24

#TCP ACK scan without ping (skip host discovery) -> weird output 
sudo nmap -Pn -sA 5.6.7.0/24