#!/bin/sh

#Sends ping to all the subnet
#Could use the broadcast address but this is preferred
for i in $(seq 255)
do
    ping -c1 5.6.7.$i
done