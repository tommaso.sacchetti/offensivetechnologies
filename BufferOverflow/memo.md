The webserver.c source contains (among the others) two buffer overflow vulnerabilities which can cause DoS and/or RCE:

- The first vulnerability is inside the get_header() method with the two functions memcpy() (line 88) and strcpy() (line 92). Here the actual size of the header is not checked against the size of the buffer initializet at line 87.
To fix this we add a bound check to the header field length and if it exceeds our limit we return zero, so the function will answer with a HTTP CODE 400 Bad Request.

- The second is at line 313 in the send_response() method with the execution of a strcat() which concatenates the path to the sendmessage buffer. To avoid this we can use different methods, I decided to replace the strcat() function used to concatenate the path with a strncat()  which allows us to add a limit to the number of bytes that it will concatenates. Moreover, since under Linux the maximum path length is 4096 bytes, I changed the buffer size to cover that.

The demo exploit available in the attached bof.py file sends a HTTP POST request to the webserver with the HTTP header field "Content-Length" containing a 1336 byte size string.
Using this vulnerability I was able to open a reverse shell to the server and since the program was running with root permission I had full unrestricted access to the machine.

The breach was really serious, as said before it could have allowed attackers to gain full control of our server and the data contained in it. Moreover it could have been infected and become part of a botnet.
First we need to put the server offline, then inspect the logs to look for anomalies and eventually try to find if and from where these vulnerabilities were exploited. Then we should reset the entire system to an untouched or default state and reset any sensitive info, then make sure to apply the patch to the server.