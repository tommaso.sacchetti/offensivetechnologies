The FCCU.php source contains a SQL injection vulnerability in the login form which allows anyone to log in as any bank user and and perform operations on behalf their behalf.

To fix the vulnerability any user's input must be validated before using it in a SQL query, for this reason I changed all the queries that use external input with prepared statements that can filter those inputs.
Moreover, for the login form I inserted a check to allow only alphanumeric characters in the password field and only integers as account numbers.

The SQLi vulnerability is solved simply with the prepared statements which do not accept SQL commands as parameters. The initial filtering is made to be coherent with the "alphanumeric only" voice nearby the password field.

This breach exposed users data privacy and integrity, moreover it allowed for user impersonation in the context of the bank.
However it was not possible to gain root access nor to arbitrarily interact with the database.
To secure the server (FCCU.php only) is is sufficient to patch the source with the attached FCCU.patch file.