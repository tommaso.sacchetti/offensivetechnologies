The memo.pl source contains a directory traversal vulnerability due to absent path validation and a privilege escalation vulnerability due to a CGI wrapper which has SUID-root permissions.

The demo exploit is pretty simple, we can use the lack of path validation and the root privileges to read the /etc/shadow and copy it locally issuing the command:

`curl --data "memo=/etc/shadow" http://localhost:8888/cgi-bin/memo.cgi > shadow.txt`

To fix the privilege escalation we must either delete the CGI wrapper and execute directly the Perl script or remove the SUID-root permission to the memo.cgi file, I chose the latter.
To fix the directory traversal vulnerability we must add input validation in the memo.pl source before the execution of the open command, we need two different filters for the path:

1. Allow only paths starting with /home/*/memo/ or /root/memo/

2. Remove any potentially malicious HTML tag and the ability to backtrack with /..

Both are achieved using regular expressions. 
It is not sufficient to only check (1.) because of the possibility to backtrack. These fixes can be observed in the attached memo.patch file.

Changing the permission as described above is enough to have a fully working memo reader application without using SUID-root, since the memos contained in the root folder are readable without root privileges.
(not sure if they should be though)

The breach was serious, an unauthorized user could have executed any command or accessed any file with root privileges.
Theoretically a reverse shell could be opened from the server to the attacker machine giving full remote control over the server.

Since we do not know what kind of modifications were done using this exploit, it is not sufficient to change the /etc/shadow file permissions and apply the patch, the whole server should be set-up from scratch again.